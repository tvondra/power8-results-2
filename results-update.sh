#!/bin/sh

DB=$1
PREFIX=$2



./results-load.sh "$DB" pgbench-2000-unlogged-async-noskip
./results-load.sh "$DB" pgbench-2000-unlogged-async-skip
./results-load.sh "$DB" dilip-2000-unlogged-async

./results-load.sh "$DB" pgbench-2000-logged-sync-noskip
./results-load.sh "$DB" pgbench-2000-logged-sync-skip
./results-load.sh "$DB" dilip-2000-logged-sync

./results-load.sh "$DB" pgbench-2000-unlogged-sync-noskip
./results-load.sh "$DB" pgbench-2000-unlogged-sync-skip
./results-load.sh "$DB" dilip-2000-unlogged-sync

./results-load.sh "$DB" pgbench-300-logged-async-noskip
./results-load.sh "$DB" pgbench-300-logged-async-skip
./results-load.sh "$DB" dilip-300-logged-async

./results-load.sh "$DB" pgbench-300-unlogged-async-noskip
./results-load.sh "$DB" pgbench-300-unlogged-async-skip
./results-load.sh "$DB" dilip-300-unlogged-async

./results-load.sh "$DB" pgbench-300-logged-sync-noskip
./results-load.sh "$DB" pgbench-300-logged-sync-skip
./results-load.sh "$DB" dilip-300-logged-sync

./results-load.sh "$DB" pgbench-300-unlogged-sync-noskip
./results-load.sh "$DB" pgbench-300-unlogged-sync-skip
./results-load.sh "$DB" dilip-300-unlogged-sync

./results-load.sh "$DB" dilip-300-unlogged-sync-tmpfs
./results-load.sh "$DB" pgbench-300-unlogged-sync-noskip-tmpfs
./results-load.sh "$DB" pgbench-300-unlogged-sync-skip-tmpfs

./results-load.sh "$DB" dilip-300-logged-sync-tmpfs
./results-load.sh "$DB" pgbench-300-logged-sync-skip-tmpfs
./results-load.sh "$DB" pgbench-300-logged-sync-noskip-tmpfs

./results-load.sh "$DB" pgbench-300-unlogged-async-skip-tmpfs
./results-load.sh "$DB" dilip-300-unlogged-async-tmpfs

./results-analyze.sh "$DB" "$PREFIX"
./results-analyze-by-test.sh "$DB" "$PREFIX"
