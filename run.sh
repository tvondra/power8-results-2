#!/bin/sh

for s in 300 2000; do

	for c in sync async; do

		for w in unlogged logged; do

			# run the benchmark
			if [ ! -d "dilip-$s-$w-$c" ]; then

				echo `date +%s` [`date`] "dilip scale=$s wal=$w commit=$c"

				./run-pgbench.sh "dilip-$s-$w-$c" $s $w postgresql-$c.conf "" dilip-$s.sql > dilip-$s-$w-$c.log 2>&1

				# update the repo (in case someone pushed processed results or such)
				git pull > /dev/null 2>&1

				# push to git repo
				git add bench.log > /dev/null 2>&1
				git add dilip-$s-$w-$c.log dilip-$s-$w-$c > /dev/null 2>&1
				git commit -m "dilip-$s-$w-$c" > /dev/null 2>&1
				git push > /dev/null 2>&1

			fi

			for n in skip noskip; do

				# run the benchmark
				if [ ! -d "pgbench-$s-$w-$c-$n" ]; then

					echo `date +%s` [`date`] "pgbench scale=$s wal=$w commit=$c skip=$n"

					./run-pgbench.sh "pgbench-$s-$w-$c-$n"	$s $w postgresql-$c.conf $n "" > pgbench-$s-$w-$c-$n.log 2>&1

					# update the repo
					git pull > /dev/null 2>&1

					# push to git repo
					git add bench.log > /dev/null 2>&1
					git add pgbench-$s-$w-$c-$n.log pgbench-$s-$w-$c-$n > /dev/null 2>&1
					git commit -m "pgbench-$s-$w-$c-$n" > /dev/null 2>&1
					git push > /dev/null 2>&1

				fi

			done

		done

	done

done
